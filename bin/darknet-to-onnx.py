#!/usr/bin/env python3
"""
darknet2onnx {} - Convert a darknet yolov4/v7 model to ONNX.

See https://gitlab.kitware.com/keu-computervision/ml/darknet-to-pytorch-onnx for source code and documentation.
"""
import argparse

try:
    from darknet2onnx.__init__ import __version__
except ImportError:
    from sys import path
    from pathlib import Path
    path.append(str(Path(__file__).parents[1]))

    from darknet2onnx.__init__ import __version__

from darknet2onnx.export import export_darknet_to_onnx
from darknet2onnx.darknet2pytorch.model import Darknet


def get_parser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__.format(__version__))
    parser.add_argument(
        "--cfg",
        dest="cfg_file",
        help="Path to darknet config (.cfg) file")
    parser.add_argument(
        "--weights",
        dest="weights_file",
        help="Path to darknet weights (.weights) file")
    parser.add_argument(
        "--batch-size",
        dest="batch_size",
        type=int,
        help="Batch size for the converted ONNX model (default: 1)")

    return parser


def main(cfg_file, weights_file, batch_size=1):
    model = Darknet(cfg_file)
    model.load_weights(weights_file)
    export_darknet_to_onnx(model, batch_size)


if __name__ == '__main__':
    args = get_parser().parse_args()
    main(**vars(args))
