#!/usr/bin/env python3
"""
darknet2onnx {} demo - Convert a darknet yolov4/v7 model and run ONNX inference on image.

See https://gitlab.kitware.com/keu-computervision/ml/darknet-to-pytorch-onnx for source code and documentation.
"""
import argparse

try:
    from darknet2onnx.__init__ import __version__
except ImportError:
    from sys import path
    from pathlib import Path
    path.append(str(Path(__file__).parents[1]))

    from darknet2onnx.__init__ import __version__

from darknet2onnx.detect import detect_onnx
from darknet2onnx.visualization import load_label_names, plot_boxes_cv2
from darknet2onnx.export import export_darknet_to_onnx
from darknet2onnx.darknet2pytorch.model import Darknet


def get_parser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__.format(__version__))
    parser.add_argument(
        "--cfg",
        dest="cfg_file",
        help="Path to darknet config (.cfg) file")
    parser.add_argument(
        "--labels",
        dest="labels_file",
        help="Path to darknet labels (.lbl) files")
    parser.add_argument(
        "--weights",
        dest="weights_file",
        help="Path to darknet weights (.weights) file")
    parser.add_argument(
        "--image",
        dest="image_path",
        help="Path to image file")
    parser.add_argument(
        "--batch-size",
        dest="batch_size",
        type=int,
        help="Batch size for the converted ONNX model (default: 1)")

    return parser


def main(cfg_file, labels_file, weights_file, image_path, batch_size=1):
    # model instantiation and inference
    model = Darknet(cfg_file)
    model.load_weights(weights_file)
    onnx_file = export_darknet_to_onnx(model, batch_size)
    boxes = detect_onnx(onnx_file, image_path)
    # visualization
    label_names = load_label_names(labels_file)
    for ii in range(boxes.shape[0]):
        class_id = boxes[ii, 5].astype(int)
        class_conf = boxes[ii, 4]
        bbox = boxes[ii, :4]
        print(f"{label_names[class_id]}: {bbox} ({class_conf:.3f})")
    plot_boxes_cv2(image_path, boxes, savename="predictions_onnx.jpg", label_names=label_names)


if __name__ == '__main__':
    args = get_parser().parse_args()
    main(**vars(args))
