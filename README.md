# darknet-pytorch-to-onnx

A minimal PyTorch implementation of YOLOv4.
- Paper Yolo v4: https://arxiv.org/abs/2004.10934
- Source code:https://github.com/AlexeyAB/darknet
- More details: http://pjreddie.com/darknet/yolo/

# Requirements

* numpy<2.0
* torch>=1.6.0

# Installation

```sh
git clone https://gitlab.kitware.com/keu-computervision/ml/darknet-to-pytorch-onnx.git
cd darknet-to-pytorch-onnx
pip install .
```

To install optionnal dependencies, used for the demo:
```sh
pip install -r requirements-opt.txt
```

# Usage

## Scripts

```sh
bin/darknet-to-onnx.py --help
```

```
optional arguments:
  -h, --help            show this help message and exit
  --cfg CFG_FILE        Path to darknet config (.cfg) file
  --weights WEIGHTS_FILE
                        Path to darknet weights (.weights) file
  --batch-size BATCH_SIZE
                        Batch size for the converted ONNX model (default: 1)
```

We also provides the script `bin/darknet-to-onnx-demo.py` to run the ONNX inference.

## Python API

```python
from pathlib import Path
from darknet2onnx.export import export_darknet_to_onnx
from darknet2onnx.darknet2pytorch.model import Darknet

cfg_file = Path("/path/to/darknet/.cfg")
weights_file = Path("/path/to/darknet/.weights")

model = Darknet(cfg_file)
model.load_weights(weights_file)
export_darknet_to_onnx(model, batch_size=1)
```

## Outputs

The converted darknet model into [ONNX](https://onnx.ai/) format.
```
.
├─ predictions_onnx.jpg
└─ yolov4_1_3_640_640.onnx
```

# Related Projects

- https://github.com/Tianxiaomo/pytorch-YOLOv4 (most support)
- https://github.com/WongKinYiu/PyTorch_YOLOv4.git (did not work)
- https://github.com/jkjung-avt/tensorrt_demos.git (NVIDIA license)
- https://github.com/NVIDIA-AI-IOT/yolo_deepstream.git (requires the onnx model)

# Reference

```
@article{bochkovskiy2020yolov4,
  title={Yolov4: Optimal speed and accuracy of object detection},
  author={Bochkovskiy, Alexey and Wang, Chien-Yao and Liao, Hong-Yuan Mark},
  journal={arXiv preprint arXiv:2004.10934},
  year={2020}
}
```
