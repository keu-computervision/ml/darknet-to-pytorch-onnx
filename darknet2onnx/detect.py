import time
import onnxruntime
import numpy as np
import cv2


def resize_image(img, tgt_shape, resize_method="maintain_ar"):
    """
    Resize the opencv image using an opencv resize transform.
    tgt_shape with the following format (height, width)
    Available resize options are: \"maintain_ar\", \"scale\".
    """
    tgt_height, tgt_width = tgt_shape
    img_height, img_width = img.shape[:2]

    # resizing
    scale_x = tgt_width / img_width
    scale_y = tgt_height / img_height
    if resize_method == "maintain_ar":
        scale = min(scale_x, scale_y)
        scale_x, scale_y = (scale, scale)
    resized = cv2.resize(img, dsize=None, fx=scale_x, fy=scale_y, interpolation=cv2.INTER_LINEAR)

    # this is specific to maintain_ar, but could be done by default for any resize option
    out_img = np.zeros((tgt_height, tgt_width, 3), dtype=resized.dtype)
    out_img[:resized.shape[0], :resized.shape[1]] = resized

    return out_img


def format_image_to_onnx(img, color_conversion=True):
    """
    Given an opencv image, perfoms successive transformations so it is compatible with ONNX runtime.
    The following transforms are performed:
    - color channel conversion (if needed)
    - transform array to [batch=1, n_channels, height, width]
    - cast image to float32 and rescale to [0.0, 1.0]
    """

    out_img = np.copy(img)
    if color_conversion:
        out_img = cv2.cvtColor(out_img, cv2.COLOR_BGR2RGB)
    # adding batch dimension
    out_img = np.transpose(np.expand_dims(out_img, axis=0), (0, 3, 1, 2))
    # cast to float32 and rescale
    out_img = out_img.astype(np.float32) / 255.0

    return out_img


def preprocess_image(img_bgr, tgt_shape, resize_method="maintain_ar"):
    """
    Preprocess the image for ONNX inference using specific resizing method.
    Available resize options from VIAME are: \"maintain_ar\", \"scale\".
    """
    resized_img = resize_image(img_bgr, tgt_shape[:2], resize_method)
    # darknet send BGR image instead of RGB, so disable color conversion
    # https://github.com/AlexeyAB/darknet/blob/d02cc3a81689a1593dc5d0c8ed018d10338bebdd/include/yolo_v2_class.hpp#L136
    out_img = format_image_to_onnx(resized_img, color_conversion=False)

    return out_img


def nms_cpu(boxes, confs, nms_thresh=0.5, min_mode=False):
    # print(boxes.shape)
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]

    areas = (x2 - x1) * (y2 - y1)
    order = confs.argsort()[::-1]

    keep = []
    while order.size > 0:
        idx_self = order[0]
        idx_other = order[1:]

        keep.append(idx_self)

        xx1 = np.maximum(x1[idx_self], x1[idx_other])
        yy1 = np.maximum(y1[idx_self], y1[idx_other])
        xx2 = np.minimum(x2[idx_self], x2[idx_other])
        yy2 = np.minimum(y2[idx_self], y2[idx_other])

        w = np.maximum(0.0, xx2 - xx1)
        h = np.maximum(0.0, yy2 - yy1)
        inter = w * h

        if min_mode:
            over = inter / np.minimum(areas[order[0]], areas[order[1:]])
        else:
            over = inter / (areas[order[0]] + areas[order[1:]] - inter)

        inds = np.where(over <= nms_thresh)[0]
        order = order[inds + 1]

    return np.array(keep)


def nms_boxes(conf_thresh, nms_thresh, box_array, confs):

    if type(box_array).__name__ != 'ndarray':
        box_array = box_array.cpu().detach().numpy()
        confs = confs.cpu().detach().numpy()

    num_classes = confs.shape[2]

    # [batch, num, num_classes] --> [batch, num]
    max_conf = np.max(confs, axis=2)
    max_id = np.argmax(confs, axis=2)

    bboxes_batch = []
    for i in range(box_array.shape[0]):

        argwhere = max_conf[i] > conf_thresh
        l_box_array = box_array[i, argwhere, :]
        l_max_conf = max_conf[i, argwhere]
        l_max_id = max_id[i, argwhere]

        bboxes = []
        # nms for each class
        for j in range(num_classes):

            cls_argwhere = l_max_id == j
            ll_box_array = l_box_array[cls_argwhere, :]
            ll_max_conf = l_max_conf[cls_argwhere]
            ll_max_id = l_max_id[cls_argwhere]

            keep = nms_cpu(ll_box_array, ll_max_conf, nms_thresh)

            if (keep.size > 0):
                ll_box_array = ll_box_array[keep, :]
                ll_max_conf = ll_max_conf[keep]
                ll_max_id = ll_max_id[keep]

                for k in range(ll_box_array.shape[0]):
                    bboxes.append([
                        ll_box_array[k, 0],
                        ll_box_array[k, 1],
                        ll_box_array[k, 2],
                        ll_box_array[k, 3],
                        ll_max_conf[k],
                        ll_max_id[k]
                        ])

        bboxes_batch.append(bboxes)

    return bboxes_batch


def convert_boxes(boxes, src_fmt):
    """Convert boxes from source format to standard format (x0y0x1y1)."""
    out_boxes = np.copy(boxes)

    if src_fmt != "cxcywh":
        raise Exception(f"Box conversion with {src_fmt} not supported!")

    if src_fmt == "cxcywh":
        out_boxes[..., 0] = boxes[..., 0] - boxes[..., 2] / 2
        out_boxes[..., 1] = boxes[..., 1] - boxes[..., 3] / 2
        out_boxes[..., 2] = out_boxes[..., 0] + boxes[..., 2]  # w becomes x1 = x0 + w
        out_boxes[..., 3] = out_boxes[..., 1] + boxes[..., 3]  # h becomes y1 = y0 + h

    return out_boxes


def resize_boxes(boxes, tgt_shape, resize_method="maintain_ar"):
    """Resize boxes to match input image size before pre-processing."""
    tgt_height, tgt_width = tgt_shape
    out_boxes = np.copy(boxes)

    # resizing
    scale_x = tgt_width
    scale_y = tgt_height
    if resize_method == "maintain_ar":
        scale = max(scale_x, scale_y)
        scale_x, scale_y = (scale, scale)
    out_boxes[..., 0] = boxes[..., 0] * scale_x
    out_boxes[..., 1] = boxes[..., 1] * scale_y
    out_boxes[..., 2] = boxes[..., 2] * scale_x
    out_boxes[..., 3] = boxes[..., 3] * scale_y

    return out_boxes


def postprocess_boxes(boxes, probs, confs, tgt_shape, resize_method="maintain_ar", conf_thresh=0.01):
    """
    Postprocess the boxes fater darknet ONNX inference.
    The following transforms are performed:
        - filter boxes using confidence (objectness) as in darknet
        - transform array to [batch=1, n_channels, height, width]
        - cast image to float32 and rescale to [0.0, 1.0]
    """
    # darknet confidence filtering
    # https://github.com/AlexeyAB/darknet/blob/d02cc3a81689a1593dc5d0c8ed018d10338bebdd/src/yolo_layer.c#L1103
    idx_to_keep = (confs > conf_thresh).flatten()
    boxes = boxes[:, idx_to_keep]
    probs = probs[:, idx_to_keep]
    confs = confs[:, idx_to_keep]

    # box format conversion to (x0, y0, x1, y1)
    fmt_boxes = convert_boxes(boxes, "cxcywh")

    # NMS
    filtered_boxes = np.stack(nms_boxes(0.0, 0.0, fmt_boxes, probs*confs))

    # resize boxes to img
    out_boxes = resize_boxes(filtered_boxes, tgt_shape, resize_method)
    out_boxes = out_boxes.reshape(-1, 6)

    return out_boxes


def detect_onnx(onnx_file, image_path):
    """ Load the image and run a single detection from an onnx session."""
    RESIZING_METHOD = "maintain_ar"
    CONF_THRESH = 0.01

    # for tensorrt python to be found, add tensorrt_libs and tensorrt_bindings to LD_LIBRARY_PATH
    onnx_session = onnxruntime.InferenceSession(onnx_file, providers=[
            "TensorrtExecutionProvider", "CUDAExecutionProvider", "CPUExecutionProvider"
            ])
    net_height, net_width = onnx_session.get_inputs()[0].shape[2:4]
    print("Input netowrk shape: ", (net_height, net_width))

    # image loading and preprocessing
    st = time.time()
    image_src = cv2.imread(image_path)
    img_in = preprocess_image(image_src, (net_height, net_width), resize_method=RESIZING_METHOD)
    print(f"Pre-processing time: {1000*(time.time() - st)}")

    # run ONNX inference
    st = time.time()
    input_name = onnx_session.get_inputs()[0].name
    outputs = onnx_session.run(None, {input_name: img_in})
    boxes = outputs[0]
    probs = outputs[1]
    confs = outputs[2]
    print(f"Inference time: {1000*(time.time() - st)}")

    # post-processing and image saving
    st = time.time()
    tgt_shape = image_src.shape[:2]
    boxes = postprocess_boxes(boxes, probs, confs, tgt_shape, conf_thresh=CONF_THRESH, resize_method=RESIZING_METHOD)
    print(f"Post-processing time: {1000*(time.time() - st)}ms")

    return boxes
