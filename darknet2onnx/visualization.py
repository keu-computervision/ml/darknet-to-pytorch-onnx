import numpy as np
import cv2


def plot_boxes_cv2(image_path, boxes, savename=None, label_names=None, color=None):
    img = cv2.imread(image_path)
    colors = np.array([[1, 0, 1], [0, 0, 1], [0, 1, 1], [0, 1, 0], [1, 1, 0], [1, 0, 0]], dtype=np.float32)

    def get_color(c, x, max_val):
        ratio = float(x) / max_val * 5
        i = int(np.floor(ratio))
        j = int(np.ceil(ratio))
        ratio = ratio - i
        r = (1 - ratio) * colors[i][c] + ratio * colors[j][c]
        return int(r * 255)

    # remove batch dim
    pred_boxes = boxes[:, :4]
    pred_confs = boxes[:, 4]
    pred_classes = boxes[:, 5]

    width = img.shape[1]
    height = img.shape[0]
    for i in range(len(pred_boxes)):
        box = pred_boxes[i].astype(int)
        bbox_thick = int(0.6 * (height + width) / 600)
        if color:
            rgb = color
        else:
            rgb = (255, 0, 0)
        if label_names:
            x1, y1, x2, y2 = box
            cls_conf = pred_confs[i]
            cls_id = pred_classes[i].astype(int)
            classes = len(label_names)
            offset = cls_id * 123457 % classes
            red = get_color(2, offset, classes)
            green = get_color(1, offset, classes)
            blue = get_color(0, offset, classes)
            if color is None:
                rgb = (red, green, blue)
            msg = str(label_names[cls_id])+" "+str(round(cls_conf,3))
            t_size = cv2.getTextSize(msg, 0, 0.7, thickness=bbox_thick // 2)[0]
            c1, c2 = (x1,y1), (x2, y2)
            # print(c1)
            # print(c2)
            c3 = (c1[0] + t_size[0], c1[1] - t_size[1] - 3)
            cv2.rectangle(img, c1, c3, rgb, -1)
            img = cv2.putText(img, msg, (c1[0], c1[1] - 2), cv2.FONT_HERSHEY_SIMPLEX,0.7, (0.0,0.0,0.0), bbox_thick//2,lineType=cv2.LINE_AA)

        img = cv2.rectangle(img, c1, c2, rgb, bbox_thick)

    if savename:
        print("save plot results to %s" % savename)
        cv2.imwrite(savename, img)

    return img


def load_label_names(namesfile):
    label_names = []
    with open(namesfile, 'r') as fp:
        lines = fp.readlines()
    for line in lines:
        line = line.rstrip()
        label_names.append(line)
    return label_names
