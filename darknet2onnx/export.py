import torch


def export_darknet_to_onnx(model, batch_size=1, onnx_filepath=None):
    """ Export a Darknet pytorch model into ONNX format, with optionnal dynamic batch. """
    # Put darknet model in evaluation mode
    model.eval()
    print(model)

    # Check if model has dynamic batch size
    x = torch.randn((batch_size, 3, model.height, model.width), requires_grad=False)
    dynamic_axes = None
    if batch_size <= 0:
        dynamic_axes = {
            "input": {0: "batch_size"},
            "boxes": {0: "batch_size"},
            "probs":{0: "batch_size"},
            "confs": {0: "batch_size"}}
        x = torch.randn((1, 3, model.height, model.width), requires_grad=True)
    if not onnx_filepath:
        onnx_filepath = f"yolov4_{batch_size}_3_{model.height}_{model.width}.onnx"
    input_names = ["input"]
    output_names = ["boxes", "probs", "confs"]

    # Export the model
    torch.onnx.export(
        model,
        x,
        onnx_filepath,
        export_params=True,
        opset_version=11,  # pytorch 1.6 supports < 12
        do_constant_folding=True,
        input_names=input_names, output_names=output_names,
        dynamic_axes=dynamic_axes
        )
    print("Onnx model exporting done!")

    return onnx_filepath
