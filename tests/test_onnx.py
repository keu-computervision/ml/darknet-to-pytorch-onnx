import pytest
import pathlib
import torch
import onnxruntime
import numpy as np

from darknet2onnx import __version__
from darknet2onnx.export import export_darknet_to_onnx
from darknet2onnx.darknet2pytorch.model import Darknet


@pytest.fixture()
def setup_temp_dir(tmp_path_factory) -> str:
    tmp_root_dir = tmp_path_factory.mktemp(f"darknet2onnx-v{__version__}_")

    return tmp_root_dir


def test_onnx_export(setup_temp_dir):
    """Test pytorch and onnx conversion."""
    tmp_root_dir = setup_temp_dir
    cfg_filepath = pathlib.Path("configs/yolov4-csp-s-mish.cfg")

    # Load and check Darknet model ouput
    torch.manual_seed(0)
    model = Darknet(cfg_filepath)
    model.eval()
    batch_size = 1
    x = torch.randn((batch_size, 3, model.height, model.width), requires_grad=False)
    torch_outputs = model(x)
    assert len(torch_outputs) == 3
    assert torch_outputs[0].size() == (1, 25200, 4)
    assert torch_outputs[1].size() == (1, 25200, 80)
    assert torch_outputs[2].size() == (1, 25200, 1)

    # ONNX conversion
    onnx_filepath = tmp_root_dir.joinpath("test_yolov4_1_3_640_640.onnx")
    export_darknet_to_onnx(model, onnx_filepath=onnx_filepath)

    # ONNX inference
    onnx_session = onnxruntime.InferenceSession(onnx_filepath, providers=[
            "TensorrtExecutionProvider", "CUDAExecutionProvider", "CPUExecutionProvider"
            ])
    onnx_input_names = [input.name for input in onnx_session.get_inputs()]
    onnx_output_names = [output.name for output in onnx_session.get_outputs()]
    assert onnx_input_names == ["input"]
    assert onnx_output_names == ["boxes", "probs", "confs"]

    onnx_outputs = onnx_session.run(None, {onnx_input_names[0]: x.detach().cpu().numpy()})
    for output_idx in range(len(onnx_outputs)):
        np.testing.assert_almost_equal(
            onnx_outputs[output_idx],
            torch_outputs[output_idx].detach().cpu().numpy())
